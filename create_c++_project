#!/bin/bash

if [ -z "$1" ]; then
    echo "No input parameter"
    exit
fi


IFS=$'\n' read -r -d '' doxygen_search_css <<'EOF'
/*---------------- Search Box */

#FSearchBox {
    float: left;
}

#MSearchBox {
    white-space : nowrap;
    position: absolute;
    float: none;
    display: inline;
    margin-top: 8px;
    right: 10px;
    top: 10px;
    width: 170px;
    z-index: 102;
    background-color: white;
}

#MSearchBox .left
{
    /*
    display:block;
    position:absolute;
    left:10px;
    width:20px;
    height:19px;
    background:url('search_l.png') no-repeat;
    background-position:right;
    */
}

#MSearchSelect {
    display:block;
    position:absolute;
    width:20px;
    height:19px;
}

.left #MSearchSelect {
    left:4px;
    top:5px;
}

.right #MSearchSelect {
    right:5px;
}

#MSearchField {
    padding: 6px 24px;
    /*
    display:block;
    position:absolute;
    height:19px;
    background:url('search_m.png') repeat-x;
    border:none;
    width:111px;
    margin-left:20px;
    padding-left:4px;
    color: #909090;
    outline: none;
    font: 9pt Arial, Verdana, sans-serif;
    */
}

#FSearchBox #MSearchField {
    margin-left:15px;
}

#MSearchBox .right {
    /*
    display:block;
    position:absolute;
    right:10px;
    top:0px;
    width:20px;
    height:19px;
    background:url('search_r.png') no-repeat;
    background-position:left;
    */
}

#MSearchClose {
    display: none;
    position: absolute;
    top: 4px;
    background : none;
    border: none;
    margin: 0px 4px 0px 0px;
    padding: 0px 0px;
    outline: none;
}

.left #MSearchClose {
    left: 6px;
}

.right #MSearchClose {
    right: 2px;
}

.MSearchBoxActive #MSearchField {
    color: #000000;
}

/*---------------- Search filter selection */

#MSearchSelectWindow {
    /*
    display: none;
    position: absolute;
    left: 0; top: 0;
    border: 1px solid #90A5CE;
    background-color: #F9FAFC;
    z-index: 1;
    padding-top: 4px;
    padding-bottom: 4px;
    -moz-border-radius: 4px;
    -webkit-border-top-left-radius: 4px;
    -webkit-border-top-right-radius: 4px;
    -webkit-border-bottom-left-radius: 4px;
    -webkit-border-bottom-right-radius: 4px;
    -webkit-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);
    */
}

.SelectItem {
    font: 8pt Arial, Verdana, sans-serif;
    padding-left:  2px;
    padding-right: 12px;
    border: 0px;
}

span.SelectionMark {
    margin-right: 4px;
    font-family: monospace;
    outline-style: none;
    text-decoration: none;
}

a.SelectItem {
    display: block;
    outline-style: none;
    color: #000000; 
    text-decoration: none;
    padding-left:   6px;
    padding-right: 12px;
}

a.SelectItem:focus,
a.SelectItem:active {
    color: #000000; 
    outline-style: none;
    text-decoration: none;
}

a.SelectItem:hover {
    color: #FFFFFF;
    background-color: #3D578C;
    outline-style: none;
    text-decoration: none;
    cursor: pointer;
    display: block;
}

/*---------------- Search results window */

iframe#MSearchResults {
    width: 60ex;
    height: 15em;
}

#MSearchResultsWindow {
    display: none;
    position: absolute;
    left: 0; top: 0;
    border: 1px solid #000;
    background-color: #EEF1F7;
}

/* ----------------------------------- */


#SRIndex {
    clear:both; 
    padding-bottom: 15px;
}

.SREntry {
    font-size: 10pt;
    padding-left: 1ex;
}

.SRPage .SREntry {
    font-size: 8pt;
    padding: 1px 5px;
}

body.SRPage {
    margin: 5px 2px;
}

.SRChildren {
    padding-left: 3ex; padding-bottom: .5em 
}

.SRPage .SRChildren {
    display: none;
}

.SRSymbol {
    font-weight: bold; 
    color: #425E97;
    font-family: Arial, Verdana, sans-serif;
    text-decoration: none;
    outline: none;
}

a.SRScope {
    display: block;
    color: #425E97; 
    font-family: Arial, Verdana, sans-serif;
    text-decoration: none;
    outline: none;
}

a.SRSymbol:focus, a.SRSymbol:active,
a.SRScope:focus, a.SRScope:active {
    text-decoration: underline;
}

span.SRScope {
    padding-left: 4px;
}

.SRPage .SRStatus {
    padding: 2px 5px;
    font-size: 8pt;
    font-style: italic;
}

.SRResult {
    display: none;
}

DIV.searchresults {
    margin-left: 10px;
    margin-right: 10px;
}

/*---------------- External search page results */

.searchresult {
    background-color: #F0F3F8;
}

.pages b {
   color: white;
   padding: 5px 5px 3px 5px;
   background-image: url("../tab_a.png");
   background-repeat: repeat-x;
   text-shadow: 0 1px 1px #000000;
}

.pages {
    line-height: 17px;
    margin-left: 4px;
    text-decoration: none;
}

.hl {
    font-weight: bold;
}

#searchresults {
    margin-bottom: 20px;
}

.searchpages {
    margin-top: 10px;
}
EOF


IFS=$'\n' read -r -d '' bootstrap_doxygen_js <<'EOF'
(function($) {
    $.fn.changeElementType = function(newType) {
        var attrs = {};

        $.each(this[0].attributes, function(idx, attr) {
            attrs[attr.nodeName] = attr.value;
        });

        this.replaceWith(function() {
            return $("<" + newType + "/>", attrs).append($(this).contents());
        });
    };
})(jQuery);

(function($) {
    $.fn.addElementType = function(newType) {
        var attrs = {};

        $.each(this[0].attributes, function(idx, attr) {
            attrs[attr.nodeName] = attr.value;
        });

        this.replaceWith(function() {
            return $("<" + newType + "/>", attrs).append($(this).contents());
        });
    };
})(jQuery);

$( document ).ready(function() {

    $("div.headertitle").addClass("page-header");
    $("div.title").addClass("h1");
    
    $('li > a[href="index.html"] > span').before("<i class='fa fa-cog'></i> ");
    $('li > a[href="index.html"] > span').text("Settings");
    $('li > a[href="modules.html"] > span').before("<i class='fa fa-square'></i> ");
    $('li > a[href="namespaces.html"] > span').before("<i class='fa fa-bars'></i> ");
    $('li > a[href="annotated.html"] > span').before("<i class='fa fa-list-ul'></i> ");
    $('li > a[href="classes.html"] > span').before("<i class='fa fa-book'></i> ");
    $('li > a[href="inherits.html"] > span').before("<i class='fa fa-sitemap'></i> ");
    $('li > a[href="functions.html"] > span').before("<i class='fa fa-list'></i> ");
    $('li > a[href="functions_func.html"] > span').before("<i class='fa fa-list'></i> ");
    $('li > a[href="functions_vars.html"] > span').before("<i class='fa fa-list'></i> ");
    $('li > a[href="functions_enum.html"] > span').before("<i class='fa fa-list'></i> ");
    $('li > a[href="functions_eval.html"] > span').before("<i class='fa fa-list'></i> ");
    $('img[src="ftv2ns.png"]').replaceWith('<span class="label label-danger">N</span> ');
    $('img[src="ftv2cl.png"]').replaceWith('<span class="label label-danger">C</span> ');
    
    $("#navrow2>ul.tablist").addClass("nav nav-pills nav-justified");
    $("#navrow1>ul.tablist").addClass("nav nav-tabs nav-justified");
    
    $("ul.tablist").css("margin-top", "0.5em");
    $("ul.tablist").css("margin-bottom", "0.5em");
    $("li.current").addClass("active");
    $("iframe").attr("scrolling", "yes");
    
    $("#nav-path > ul").addClass("breadcrumb");
    
    $("table.params").addClass("table");
    $("div.ingroups").wrapInner("<small></small>");
    $("div.levels").css("margin", "0.5em");
    $("div.levels > span").addClass("btn btn-default btn-xs");
    $("div.levels > span").css("margin-right", "0.25em");
    
    $("table.directory").addClass("table table-striped");
    $("div.summary > a").addClass("btn btn-default btn-xs");
    $("table.fieldtable").addClass("table");
    $(".fragment").addClass("well");
    $(".memitem").addClass("panel panel-default");
    $(".memproto").addClass("panel-heading");
    $(".memdoc").addClass("panel-body");
    $("span.mlabel").addClass("label label-info");
    
    $("table.memberdecls").addClass("table");
    $("[class^=memitem]").addClass("active");
    
    $("div.ah").addClass("btn btn-default");
    $("span.mlabels").addClass("pull-right");
    $("table.mlabels").css("width", "100%")
    $("td.mlabels-right").addClass("pull-right");

    $("div.ttc").addClass("panel panel-info");
    $("div.ttname").addClass("panel-heading");
    $("div.ttdef,div.ttdoc,div.ttdeci").addClass("panel-body");

    $('#MSearchBox').find('input').addClass('form-control');
    $('#MSearchBox').find('span.right').hide();
    //$('#MSearchBox').find('span.left').find('img').hide()

    $('#MSearchSelectWindow').changeElementType('ul')
    $('#MSearchSelectWindow').html($('#MSearchSelectWindow').html().replace(/<a/g,'<li><a').replace(/\/a>/g,'/a></li>'));
    $('#MSearchSelectWindow').addClass('dropdown-menu');

    $('#top').append($('#MSearchBox').parent().html());
    $('#MSearchBox').parent().remove();
    jQuery.each( document.styleSheets, function(k,e) { if ( e.href.search('/search/search.css') != -1 ) $(document.styleSheets[k]).attr('disabled', 'disabled'); });
});
EOF

IFS=$'\n' read -r -d '' doxygen_header_html <<'EOF'
<!-- HTML header for doxygen 1.8.6-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/xhtml;charset=UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=9"/>
<meta name="generator" content="Doxygen $doxygenversion"/>
<!--BEGIN PROJECT_NAME--><title>$projectname: $title</title><!--END PROJECT_NAME-->
<!--BEGIN !PROJECT_NAME--><title>$title</title><!--END !PROJECT_NAME-->
<!-- Latest compiled and minified CSS -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<!--<script type="text/javascript" src="$relpath^jquery.js"></script>-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<script type="text/javascript" src="$relpath^../bootstrap-doxygen.js"></script>
<script type="text/javascript" src="$relpath^dynsections.js"></script>
$treeview
$search
$mathjax
<link href="$relpath^$stylesheet" rel="stylesheet" type="text/css" />
$extrastylesheet
<link rel="stylesheet" href="$relpath^../search.css">
</head>
<body>
<div id="top"><!-- do not remove this div, it is closed by doxygen! -->

<!--BEGIN TITLEAREA-->
<div id="titlearea">
<table cellspacing="0" cellpadding="0">
 <tbody>
 <tr style="height: 56px;">
  <!--BEGIN PROJECT_LOGO-->
  <td id="projectlogo"><img alt="Logo" src="$relpath^$projectlogo"/></td>
  <!--END PROJECT_LOGO-->
  <!--BEGIN PROJECT_NAME-->
  <td style="padding-left: 0.5em;">
   <div id="projectname">$projectname
   <!--BEGIN PROJECT_NUMBER-->&#160;<span id="projectnumber">$projectnumber</span><!--END PROJECT_NUMBER-->
   </div>
   <!--BEGIN PROJECT_BRIEF--><div id="projectbrief">$projectbrief</div><!--END PROJECT_BRIEF-->
  </td>
  <!--END PROJECT_NAME-->
  <!--BEGIN !PROJECT_NAME-->
   <!--BEGIN PROJECT_BRIEF-->
    <td style="padding-left: 0.5em;">
    <div id="projectbrief">$projectbrief</div>
    </td>
   <!--END PROJECT_BRIEF-->
  <!--END !PROJECT_NAME-->
  <!--BEGIN DISABLE_INDEX-->
   <!--BEGIN SEARCHENGINE-->
   <td>$searchbox</td>
   <!--END SEARCHENGINE-->
  <!--END DISABLE_INDEX-->
 </tr>
 </tbody>
</table>
</div>
<!--END TITLEAREA-->
<!-- end header part -->
EOF

IFS=$'\n' read -r -d '' doxygen_footer_html <<'EOF'
<!-- HTML footer for doxygen 1.8.6-->
<!-- start footer part -->
<!--BEGIN GENERATE_TREEVIEW-->
<div id="nav-path" class="navpath"><!-- id is needed for treeview function! -->
  <ul>
    $navpath
    <li class="footer">$generatedby
    <a href="http://www.doxygen.org/index.html">
    <img class="footer" src="$relpath^doxygen.png" alt="doxygen"/></a> $doxygenversion </li>
  </ul>
</div>
<!--END GENERATE_TREEVIEW-->
<!--BEGIN !GENERATE_TREEVIEW-->
<hr class="footer"/><address class="footer"><small>
$generatedby &#160;<a href="http://www.doxygen.org/index.html">
<img class="footer" src="$relpath^doxygen.png" alt="doxygen"/>
</a> $doxygenversion
</small></address>
<!--END !GENERATE_TREEVIEW-->
</body>
</html>
EOF


main_demo_cc="using namespace std;

int main(int argc, char ** argv){

  return 1;
}
"

IFS=$'\n' read -r -d '' main_test_cc <<'EOF'
/*
Using Catch test suits

Documentatino can be found at https://github.com/philsquared/Catch/blob/master/docs/tutorial.md

*/
#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

int main( int argc, char* const argv[] )
{
  // global setup...

  int result = Catch::Session().run( argc, argv );

  // global clean-up...

  return result;
}
EOF


mainCMakeLists="cmake_minimum_required(VERSION 2.6)
project(${1})
include(\"\${CMAKE_SOURCE_DIR}/cmake_helper_functions/cmake_functions.txt\")
create_include_dir()
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY \${CMAKE_BINARY_DIR})

add_subdirectory (lib)
add_subdirectory (demo)
add_subdirectory (test)


target_include_directories (\${PROJECT_NAME} PUBLIC \"\${CMAKE_SOURCE_DIR}/include/\")
create_include_dir()


"

libCMakeLists="add_definitions(-std=c++11)
include_directories(./)
aux_source_directory(./ SOURCES)
add_library(${1} \${SOURCES})
target_include_directories (\${PROJECT_NAME} PUBLIC \${CMAKE_CURRENT_SOURCE_DIR})

file(GLOB headers \"\${CMAKE_CURRENT_SOURCE_DIR}/*.hh\")

#add_local_library(dynamics)
#pkgconfig_add_library(${PROJECT_NAME} simbody)
#pkgconfig_add_library(${PROJECT_NAME} opensim)

foreach(header \${headers})
    get_filename_component(header \${header} NAME)
    add_include_dir_header(\${header})  
endforeach()

foreach(header \${headers})
    get_filename_component(header \${header} NAME)
    rename_dir_header_include(\${header})  
endforeach()

install_it(${PROJECT_NAME})

"
demoCMakeLists="add_definitions(-std=c++11)
include_directories(./)
aux_source_directory(./ SOURCES)
add_executable(\${PROJECT_NAME}Demo \${SOURCES})


target_link_libraries (\${PROJECT_NAME}Demo LINK_PUBLIC \${PROJECT_NAME})

#pkgconfig_add_library(simbody)
#pkgconfig_add_library(opensim)

install_it(${PROJECT_NAME}Demo)

#install(DIRECTORY ../models DESTINATION .)

"
testCMakeLists="add_definitions(-std=c++11)
include_directories(./)
include_directories(../lib/)
aux_source_directory(./ SOURCES)
add_executable(\${PROJECT_NAME}Test \${SOURCES})
target_include_directories (\${PROJECT_NAME}Test PUBLIC /usr/include/catchTestSuit)
target_link_libraries (\${PROJECT_NAME}Test LINK_PUBLIC \${PROJECT_NAME})
"

# if [ -d "$1" ]; then
#   echo "error directory already present"
#   exit
# fi

#--MAIN DIRECTORY
mkdir -p $1/include
mkdir -p $1/demo
mkdir -p $1/lib
mkdir -p $1/test
mkdir -p $1/doc


rm -f $1/CMakeLists.txt
rm -f $1/doc/footer.html
rm -f $1/doc/header.html
rm -f $1/doc/bootstrap-doxygen.js
rm -f $1/doc/search.css
rm -f $1/demo/CMakeLists.txt
rm -f $1/lib/CMakeLists.txt
rm -f $1/test/CMakeLists.txt



echo "$mainCMakeLists" > $1/CMakeLists.txt


echo "$doxygen_footer_html" >  $1/doc/footer.html
echo "$doxygen_header_html" >  $1/doc/header.html
echo "$bootstrap_doxygen_js" > $1/doc/bootstrap-doxygen.js
echo "$doxygen_search_css" >   $1/doc/search.css

echo "$demoCMakeLists" > $1/demo/CMakeLists.txt
echo "$main_demo_cc" >   $1/demo/main.cc
echo "$libCMakeLists" > $1/lib/CMakeLists.txt
echo "$testCMakeLists" > $1/test/CMakeLists.txt
echo "$main_test_cc" >   $1/test/main.cc


cd $1
git init
git submodule add https://gitlab.com/srill-fb99/cmake_helper_functions.git