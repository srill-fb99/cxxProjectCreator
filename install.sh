#!/bin/bash

if [[ $1 == "local" ]]; then
	cp create_c++_project ~/.local/usr/bin/

elif [[ $1 == "system" ]]; then
	sudo cp create_c++_project /usr/local/bin
else
	echo "option needed:"
	echo "   -> local : install under ~/.local/usr/bin"
	echo "   -> system : install under /usr/local/bin"
fi

